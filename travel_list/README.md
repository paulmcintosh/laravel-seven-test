### READ ME ###

## Technical Test Instructions ##

## Laravel Framework 7.30.4 ##

*** Test 1 ***
* After reviewing test the issue is base on multi-level nested views. The solution requires a change of approach. Example below
```
public function action_index()
{
    $this->layout->nest('content', View::make('home.index')->nest('submodule', 'partials.stuff'));
}
``` 
*** A psuedo template can be found in /views/contacts/nested


* Create new vhost example: http://mytest.com

* Database credentials in .env file

* Run composer to download all dependancies

* Run migration command

## Contacts Page ##

* http://mytest.com/contacts

* Navigate records with pagination menu

* Note refresh page if using search functionality

## Create Contacts Page ##

* http://mytest.com/contacts/create

* Form field validation handle by controller

