<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

interface Services {

    public function GetRecord();

    public function SetRecord();
}

class Service extends Model implements Services {

    public $data;

    public function GetRecord() {

        return $this -> data;

    }

    public function SetRecord() {

        $this -> data = 'Wolverine';

    }

}

class Todo extends Service {
    //
    public function __construct($value = '') {

        $this -> SetRecord();

    }

    public function getNav() {

        return array('home', 'about', 'people', 'branches');

    }

    public function getResults() {

        return array('xmen', 'avengers', 'defenders', 'powerpack');

    }

}
