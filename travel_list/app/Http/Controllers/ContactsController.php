<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = '/contacts/index';
        
        $contacts = Contacts::paginate(2);

        return view($view,compact('contacts'));
    }
    
    public function fetch_data(Request $request){
        
        if($request->ajax()){
            
            $contacts = Contacts::paginate(2);
            
            return view('/contacts/partials/contact',compact('contacts'))->render();
        }
    }
    
    public function search(Request $request){
        
        if($request->ajax()){
            
            $search = $request->get('query');
            
            if($search != ''){

                $contacts = Contacts::where('firstname', 'like', '%' . $search . '%')
                ->orWhere('lastname', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->get();
                            
                $output =  view('/contacts/partials/search_contact',compact('contacts'))->render();
                
                $data = array(
                    'table_data'=>$output,
                    'total_data'=>'foo'
                );
                
                echo json_encode($data);
            }
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required | email',
            'title'=>'required'
        ]);

        Contacts::create($data);
        return Redirect::to('/contacts')->withSuccess('Contact sucessfully saved');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function show(Contacts $contacts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function edit(Contacts $contacts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacts $contacts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacts $contacts)
    {
        //
    }
}
