<?php

namespace App\Http\Controllers;

use App\Patterns;
use Illuminate\Http\Request;

class PatternsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patterns  $patterns
     * @return \Illuminate\Http\Response
     */
    public function show(Patterns $patterns)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patterns  $patterns
     * @return \Illuminate\Http\Response
     */
    public function edit(Patterns $patterns)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patterns  $patterns
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patterns $patterns)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patterns  $patterns
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patterns $patterns)
    {
        //
    }
}
